import socket
import binascii
import json

# No TLS support yet
class HighscoreClient():
    def __init__(self, username:str, gamename:str, hostname:str="151.216.202.197", port:int=80):
        self.addr = socket.getaddrinfo(hostname, port)[0][-1]
        self.username = username
        self.game = gamename

    @staticmethod
    def load_username():
        try:
            with open("/flash/nick.json") as nick:
                jsondata = nick.read()
                data = json.loads(jsondata)
                if "name" in data and type(data["name"]) == str:
                    username = data["name"]
                else:
                    username = "Anon"
        except:
            username = "Anon"
        return username

    def _encode(self, string):
        encoded = ""
        for char in string:
            encoded += "%" + binascii.hexlify(char).decode()
        return encoded

    def _decode(self, string):
        decoded = ""
        for char in string.split("%")[1:]:
            decoded += binascii.unhexlify(char).decode()
        return decoded

    def _request(self, path):
        sock = socket.socket()
        sock.connect(self.addr)
        
        sock.send(b"GET " + path + " HTTP/1.0\r\n\r\n")

        response = sock.read()

        sock.close()
        data = json.loads(self._decode(response.decode().split("\r\n\r\n")[-1])) 
        return data

    def get_games(self):
        data = self._request("/games")
        return data

    def get_user_highscore(self, game:str=None, username:str=None):
        data = self._request("/score/" + self._encode(game or self.game) + "/" + self._encode(username or self.username))
        return data

    def set_user_highscore(self, score:int, game:str=None, username:str=None):
        data = self._request("/score/" + self._encode(game or self.game) + "/" + self._encode(username or self.username) + "/" + self._encode(str(score)))
        return data

    def get_game_highscore(self, game:str=None, limit:int=10):
        data = self._request("/score/" + self._encode(game or self.game) + "?limit=" + str(limit))
        return data

if __name__ == "__main__":
    username = HighscoreClient.load_username()
    client = HighscoreClient(username=username, gamename="test")
    client.get_games()
    client.get_user_highscore()
    client.set_user_highscore(100)